<?php
// Create folder upload
if (!file_exists('C:\xampp\htdocs\day12\upload')) {
    mkdir('C:\xampp\htdocs\day12\upload', 0777, true);
}

date_default_timezone_set("Asia/Ho_Chi_Minh");
$time = date("_YmdHis");

$gender_array = array(
    "Nam",
    "Nữ"
);

$faculty_array = array(
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
);

$error_name = "";
$filenameFormat = "";
session_start();

function validateDate($date, $format = 'd/m/Y')
{
    $datetimeObj = DateTime::createFromFormat($format, $date);
    return $datetimeObj && $datetimeObj->format($format) === $date;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!isset($_POST['name']) || $_POST['name'] == null) {
        $error_name .= "Hãy nhập tên.<br>";
    }

    if (!isset($_POST['gender']) || $_POST['gender'] == null) {
        $error_name .= "Hãy chọn giới tính.<br>";
    }

    if (!isset($_POST['faculty']) || $_POST['faculty'] == null) {
        $error_name .= "Hãy chọn phân khoa.<br>";
    }

    if (!isset($_POST['birthday']) || $_POST['birthday'] == null) {
        $error_name .= "Hãy nhập ngày sinh.<br>";
    } else if (!validateDate($_POST['birthday'])) {
        $error_name .= "Hãy nhập ngày sinh đúng định dạng.";
    }

    if (
        $_POST['name'] != null
        && isset($_POST['gender'])
        && $_POST['faculty'] != null
        && $_POST['birthday'] != null
        && validateDate($_POST['birthday'])
    ) {
        header("location: submit-register.php");
        $target_dir = "upload/";
        $tail = explode(".", $_FILES["image"]["name"]); // tail of file
        $filenameFormat = pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME) . $time . "." . end($tail); // change file name
        $target_file = $target_dir . $filenameFormat;
        move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
    }

    // print_r($_FILES['image']);

    $_SESSION = $_POST;
    $_SESSION['image'] = $filenameFormat;
}
$_SESSION['gender_array'] = $gender_array;
$_SESSION['faculty_array'] = $faculty_array;

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Bootstrap -->
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen">

    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="theme-register.css">
</head>

<body>
    <div class="wrapper">
        <div class="box-register">
            <form action="" method="post" enctype="multipart/form-data">
                <div class="error">
                    <?php
                        echo $error_name
                    ?>
                </div>

                <div class="full-name">
                    <label for="" class="full-name-label">Họ và tên</label>
                    <input name="name" type="text" class="full-name-input">
                </div>

                <div class="gender">
                    <label for="" class="gender-label">Giới tính</label>

                    <?php
                    for ($i = 0; $i < count($gender_array); $i++) {
                        echo "
                            <input type=\"radio\" class=\"gender-input\" name=\"gender\" value=\"$i\">{$gender_array[$i]}
                            ";
                    }
                    ?>
                </div>

                <div class="faculty">
                    <label for="" class="faculty-lable">Phân khoa</label>

                    <select name="faculty" id="" class="faculty-select">
                        <option value=""></option>
                        <?php
                        foreach (array_keys($faculty_array) as $short_name) {
                            echo "
                                <option value=\"$short_name\">{$faculty_array[$short_name]}</option>
                            ";
                        }
                        ?>
                    </select>
                </div>

                <div class="birthday">

                    <script type="text/javascript">
                        $(function() {
                            $('#txt_date').datepicker({
                                format: "dd/mm/yyyy"
                            });
                        });
                    </script>

                    <label for="" class="birthday-label">Ngày sinh</label>

                    <input id="txt_date" name="birthday" type="text" class="birthday-input" placeholder="dd/mm/yyyy">
                </div>

                <div class="address">
                    <label for="" class="address-label">Địa chỉ</label>

                    <input type="text" name="address" class="address-input">
                </div>

                <div class="image">
                    <script>
                        function validateFileType(input) {
                            var fileName = input.files[0].name;
                            var idxDot = fileName.lastIndexOf(".") + 1;
                            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
                            if (extFile == "jpg" || extFile == "jpeg" || extFile == "png") {
                                // readURL(input);
                            } else {
                                document.getElementById("image_file").value = "";
                                alert("Chỉ hỗ trợ định dạng ảnh!");
                            }
                        }
                    </script>

                    <label for="" class="image-label">Hình ảnh</label>

                    <input type="file" name="image" id="image_file" class="image-input" accept=".jpg,.jpeg,.png" onchange="validateFileType(this)">
                </div>

                <div class="btn-register">
                    <button type="submit">Đăng ký</button>
                </div>
            </form>
        </div>
    </div>
</body>

</html>