<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Submit Register</title>
    <link rel="stylesheet" href="theme-submit-register.css">
</head>

<body>
    <div class="wrapper">
        <div class="box-submit">
            <form action="complete-regist.php" method="POST" enctype="multipart/form-data">
                <div class="full-name">
                    <label for="" class="full-name-label">Họ và tên</label>
                    <?php
                    echo $_SESSION['name'];
                    ?>
                </div>

                <div class="gender">
                    <label for="" class="gender-label">Giới tính</label>
                    <?php
                    echo $_SESSION['gender_array'][$_SESSION['gender']];
                    ?>
                </div>

                <div class="faculty">
                    <label for="" class="faculty-label">Phân khoa</label>

                    <?php
                    echo $_SESSION['faculty_array'][$_SESSION['faculty']];
                    ?>
                </div>

                <div class="birthday">
                    <label for="" class="birthday-label">Ngày sinh</label>

                    <?php
                    echo $_SESSION['birthday'];
                    ?>
                </div>

                <div class="address">
                    <label for="" class="address-label">Địa chỉ</label>

                    <?php
                    echo $_SESSION['address'];
                    ?>
                </div>

                <div class="image">
                    <label for="" class="image-label">Hình ảnh</label>
                    <?php
                    $nameImage = $_SESSION['image'];
                    echo "<img class=\"image-result\" src=\"upload/$nameImage\" alt=\"\" title=\"\" />";
                    ?>
                </div>

                <div class="btn-submit">
                    <button class="submit-btn">
                        Xác nhận
                    </button>
                </div>
        </div>
    </div>
</body>

</html>