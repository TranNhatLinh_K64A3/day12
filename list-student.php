<?php
session_start()

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home Student</title>
    <link rel="stylesheet" href="theme-list-student.css">
</head>

<body>
    <div class="wrapper">
        <div class="container-content">
            <div class="search-box">
                <div class="faculty">
                    <label for="" class="faculty-label">Khoa</label>
                    <select name="" id="" class="select-faculty">
                        <option value=""></option>
                        <?php
                        // foreach (array_keys($_SESSION['faculty']) as $short_name) {
                        //     echo "
                        //         <option value=\"$short_name\">{$_SESSION['faculty'][$short_name]}</option>
                        //     ";
                        // }
                        ?>
                    </select>
                </div>
    
                <div class="key-word">
                    <label for="" class="keyword-label">Từ khóa</label>
                    <input type="text">
                </div>

                <button class="button-3">Xóa</button>

                <button class="button-2">Tìm kiếm</button>
            </div>
    
            <div class="info">
                <label for="" class="info-label">Số sinh viên tìm thấy: <?php $sum ?></label>
            </div>
    
            <div class="btn-add-layout">
                <button class="button-2">Thêm</button>
            </div>
    
            <div class="table-list-student">
                <table>
                    <tr>
                        <th>No</th>
                        <th>Tên sinh viên</th>
                        <th>Khoa</th>
                        <th>Action</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Nguyễn Văn A</td>
                        <td>Khoa học máy tính</td>
                        <td>
                            <div class="btn-action">
                                <button>Xóa</button>
                                <button>Sửa</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Nguyễn Văn A</td>
                        <td>Khoa học máy tính</td>
                        <td>
                            <div class="btn-action">
                                <button>Xóa</button>
                                <button>Sửa</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Nguyễn Văn A</td>
                        <td>Khoa học máy tính</td>
                        <td>
                            <div class="btn-action">
                                <button>Xóa</button>
                                <button>Sửa</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Nguyễn Văn A</td>
                        <td>Khoa học máy tính</td>
                        <td>
                            <div class="btn-action">
                                <button>Xóa</button>
                                <button>Sửa</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</body>

</html>